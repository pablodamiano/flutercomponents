import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget{
@override
createState(){

return _ContadorPageState();

}


}

class _ContadorPageState extends State<ContadorPage>{

int _cantidad = 0; 
final _estilotexto = new TextStyle(fontSize: 25);

@override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text("Esta Es La App De Chapo",
        ),
        centerTitle:true,
        //backgroundColor:Color.fromARGB(6, 6, 6, 6),
      ),
      body:Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
          Text('Numero De Clicks',
            style:_estilotexto,
          ),
          Text('$_cantidad',
            style:_estilotexto,
          ),
        ],
        )
      ),
      floatingActionButton:_crearbotones(),
    );
  }

  Widget _crearbotones(){

        return Row(
           mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SizedBox(width:30.0,),
            FloatingActionButton(onPressed:_reset, child:Icon(Icons.exposure_zero ) ,),
            Expanded(child:SizedBox(width:0.0,)),
            FloatingActionButton(onPressed:_sustraer, child:Icon(Icons.remove), ),
            SizedBox(width: 10.0,),
            FloatingActionButton(onPressed:_sumar, child:Icon(Icons.add), )
          ],
        );
  }
  void _sumar(){
    setState(() {
       _cantidad++;
    });
  }
   void _sustraer(){
    setState(() {
       _cantidad--;
    });
  }
   void _reset(){
    setState(() {
       _cantidad = 0;
    });
  }
}